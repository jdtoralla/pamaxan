import { Application } from 'express';
import express = require("express");
import bodyParse = require('body-parser');
const cors = require('cors');
const path = require('path')

const login = require('./routes/login.routes');
const business = require('./routes/business.routes');

const app: Application = express();
// const PORT: number = 3000
// const serveURL = process.env.URL_IPADDRES || 'localhost';


type User = {
    name: string;
    age: number;
};
const miNumero: number = 0

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }));


app.use(cors())

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Access-Control-Allow-Credentials', 'false');
    next();
});

app.use('/api', login);
app.use('/api', business);

// if (process.env.NODE_ENV === 'production') {
//     // Serve any static files    
//     console.log('ENTRO PRODUCION');
//     app.use(express.static(path.join(__dirname, 'build')));

//     app.get('/', function (req, res) {
//         res.sendFile(path.join(__dirname, 'build', 'index.html'));
//     });
//     // app.use(express.static(path.join(__dirname, '../client/build/index.html')));
//     // res.sendFile(path.join(__dirname, 'build', 'index.html'));
// }

// const express = require('express');
// const path = require('path');
// const app = express();



// app.listen(PORT, serveURL,  () => {
//     console.log(`Server ${serveURL} listening on PORT ${process.env.PORT}`);
// }) 

// 'use stric'

if (process.env.NODE_ENV === 'production') {
    app.use(express.static(path.join(__dirname, 'build')));
    // app.use(express.static(path.join(__dirname, 'build')));

    app.get('/*', function (req, res) {
        // res.send({message: 'server ok'})
        res.sendFile(path.join(__dirname, 'build', 'index.html'));
    });
    // 
    // app.listen(9000);
}

const http = require('http')
var port = process.env.PORT
var serveURL = process.env.URL_IPADDRES

const server = http.createServer(app)

server.listen(port, serveURL, () => {
    console.log('NODE and EXPRESS running on PORT:', port + ' URL IP: ' + serveURL)
});

// module.exports = app;