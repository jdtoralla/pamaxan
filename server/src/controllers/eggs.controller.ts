
import { Request, Response } from 'express';
const sql = require('../config/db/db')

const eggType = (req: Request, res: Response) => {

    const query = `SELECT id, name FROM EggType;`
    sql.query(query, (error: any, result: any[]) => {
        if (error) {
            res.status(500).send({ error: "Error al buscar el tipo de huevo." });
        }
        res.status(200).send(result);
    })

}

const eggSize = (req: Request, res: Response) => {

    const query = `SELECT id, name FROM EggSize;`
    sql.query(query, (error: any, result: any[]) => {
        if (error) {
            res.status(500).send({ error: "Error al buscar el tipo de huevo." });
        }
        res.status(200).send(result);
    })

}

module.exports = {
    eggType, 
    eggSize
}

