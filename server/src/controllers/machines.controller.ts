
import { Request, Response } from 'express';
const sql = require('../config/db/db')

const machines = (req: Request, res: Response) => {

    const query = `SELECT id, name FROM Machine;`
    sql.query(query, (error: any, result: any[]) => {
        if (error) {
            res.status(500).send({ error: "Error al buscar la maquina." });
        }
        res.status(200).send(result);
    })
}

module.exports = {
    machines
}

