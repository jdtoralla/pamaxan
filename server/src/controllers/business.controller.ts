
import { Request, Response } from 'express';
const sql = require('../config/db/db')

const settingsBusiness = (req: Request, res: Response) => {

    const query = `SELECT * FROM Business;`
    sql.query(query, (error: any, result: any[]) => {
        if (error) {
            res.status(500).send({ error: "Error al buscar la empresa." });
        }

        res.status(200).send(result);
    })

}

module.exports = {
    settingsBusiness
}

