
import { Request, Response } from 'express';
const sql = require('../config/db/db')

const users = (req: Request, res: Response) => {

    const query = `SELECT u.id, iu.name, iu.cellPhone, iu.address, u.username, u.status, DATE_FORMAT(u.createdAt, "%m/%d/%Y  %r") as createdAt, r.description as rol FROM User u
                    INNER JOIN InfoUser iu on iu.id = u.idInfoUser
                    INNER JOIN Rol r ON r.id = u.idRol;`

    sql.query(query, (error: any, result: any[]) => {

        if (error) {
            res.status(500).send({ error: "Error al listar los usuarios." });
        }

        res.status(200).send(result);

    });


}

module.exports = {
    users
}