// const app = () => {

const login = require("../controllers/login.controller");
const users = require("../controllers/users.controller");
const eggs = require("../controllers/eggs.controller");
const machines = require("../controllers/machines.controller");
// const router = require("express").Router();
var express = require('express');
//     router.post("/", login.login);

//     app.use('/api/login', router);
// };


// module.exports = {
//     app
// }
const api = express.Router();
api.post('/login', login.login);
api.get('/users/users', users.users);


api.get('/eggs/type', eggs.eggType)
api.get('/eggs/size', eggs.eggSize)

api.get('/machines', machines.machines)

module.exports = api;