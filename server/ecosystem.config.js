module.exports = {
    apps: [{
        name: "Pamaxan",
        script: "./dist/app.js",
        watch: true,
        env_production: {
            "PORT": 3000,
            "NODE_ENV": "production",
            "URL_IPADDRES": "45.79.1.145",
            "idBusiness": 1
        },
        env_test: {
            "PORT": 3000,
            "NODE_ENV": "production",
            "URL_IPADDRES": "localhost",
            "idBusiness": 1
        }
    }]
}